---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the month of August.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## August 2023

### AMER Time Zone Webinars

#### Intro to GitLab
##### August 1st, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_hm53D4dgQ-CmJxSqWd0HeQ#/registration)

#### Intro to CI/CD
##### August 8th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_vjUyotyvSH2ZutzGQoYB2A#/registration)

#### Advanced CI/CD
##### August 15th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BXw0FvQ3QwCbhC6EeqJVmw#/registration)

#### Hands-On GitLab CI Workshop
##### August 16th, 2023 at 9:00-10:30AM Pacific Time / 12:00-1:30PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_88A3DR0SQGCMELHH9OBsDQ)

#### GitLab Administration on SaaS
##### August 22nd, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

Learn how to manage groups, projects, permissions, and more as you embark on your journey as an Owner in GitLab SaaS. In this session, you will learn what you can and cannot control and customize on the SaaS platform, and come out a SME in administration.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_dfF6s_rXQOmoccdJdV_-Ag#/registration)

#### DevSecOps/Compliance
##### August 29th, 2023 at 9:00-10AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-URHPClNQTiGaCaakwBJlw#/registration)

#### Hands-On GitLab DevSecOps Workshop
##### August 30th, 2023 at 9:00AM-12:00PM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_zKJjC90zQUmcCOHkYPxQaQ)

#### Hands-On GitLab DevSecOps Workshop
##### August 30th, 2023 at 3:00-4PM Pacific Time / 6:00-7:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_uYuCYvuhTV-raOnUaPStlg)

### EMEA Time Zone Webinars

#### Intro to GitLab
##### August 1st, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_kb0eSH4xS7i0mYjjXswH7w#/registration)

#### Intro to CI/CD
##### August 8th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_bfez_FTgQkCT4lHQZfnKUA#/registration)

#### Advanced CI/CD
##### August 15th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_UBkC-fwJT8W-Hm2uajvjDg#/registration)

#### Hands-On GitLab CI Workshop
##### August 16th, 2023 at 9:00-10:30AM UTC / 11:00AM-12:30PM CEST

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_RAZKEKEcTDW_jY4QC44riQ)

#### GitLab Administration on SaaS
##### August 22nd, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

Learn how to manage groups, projects, permissions, and more as you embark on your journey as an Owner in GitLab SaaS. In this session, you will learn what you can and cannot control and customize on the SaaS platform, and come out a SME in administration.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Rj389Kp-Tr-8P8v8QdRDmA#/registration)

#### DevSecOps/Compliance
##### August 29th, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CEST

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_qMD-pyxZQOCgB_2MJaMmnQ#/registration)

Check back later for more webinars! 
